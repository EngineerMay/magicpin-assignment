package com.assignment.activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.assignment.model.Media;
import com.assignment.utils.OnRecyclerViewItemClickListener;
import com.assignment.R;
import com.assignment.utils.Resources;
import com.assignment.adapter.VideoPlayerRecyclerAdapter;
import com.assignment.views.VideoPlayerRecyclerView;

import java.util.ArrayList;
import java.util.Arrays;

public class HomeActivity extends AppCompatActivity implements OnRecyclerViewItemClickListener {

    private VideoPlayerRecyclerView videoRecyclerView;
    public static final int VIDEO_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        videoRecyclerView = findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        videoRecyclerView.setLayoutManager(layoutManager);

        ArrayList<Media> mediaObjects = new ArrayList<>(Arrays.asList(Resources.MEDIA_OBJECTS));
        videoRecyclerView.setMediaObjects(mediaObjects);
        VideoPlayerRecyclerAdapter adapter = new VideoPlayerRecyclerAdapter(mediaObjects);
        videoRecyclerView.setAdapter(adapter);
        videoRecyclerView.setRecyclerOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        videoRecyclerView.playVideo();
    }

    @Override
    protected void onDestroy() {
        if(videoRecyclerView!=null)
            videoRecyclerView.releasePlayer();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoRecyclerView.pauseVideo();
    }

    @Override
    public void onItemClicked(Bundle bundle) {
        Intent intent = new Intent(this, FullScreenVideoActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent,VIDEO_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VIDEO_CODE && data != null && resultCode == RESULT_OK) {
            long pos = data.getLongExtra(Resources.video_pos,0);
            videoRecyclerView.setPlayerToPos(pos);

        }
    }
}
