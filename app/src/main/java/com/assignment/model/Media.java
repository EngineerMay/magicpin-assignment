package com.assignment.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Media implements Parcelable {

    private String media_url;
    private long startPosition;

    public Media(String media_url) {
        this.media_url = media_url;
        startPosition = (long) 0.0;
    }

    public Media() {
    }

    protected Media(Parcel in) {
        media_url = in.readString();
        startPosition = in.readLong();
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(media_url);
        dest.writeLong(startPosition);
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public long getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(long startPosition) {
        this.startPosition = startPosition;
    }
}










