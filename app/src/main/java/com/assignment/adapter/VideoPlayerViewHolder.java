package com.assignment.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.assignment.R;


public class VideoPlayerViewHolder extends RecyclerView.ViewHolder {

    public FrameLayout media_container;
    public ProgressBar progressBar;
    public ImageView thumbnail;
    public View parent;

    public VideoPlayerViewHolder(@NonNull View itemView) {
        super(itemView);
        parent = itemView;
        media_container = itemView.findViewById(R.id.media_container);
        progressBar = itemView.findViewById(R.id.progressBar);
        thumbnail = itemView.findViewById(R.id.thumbnail);
    }

    public void onBind() {
        parent.setTag(this);
    }

}














