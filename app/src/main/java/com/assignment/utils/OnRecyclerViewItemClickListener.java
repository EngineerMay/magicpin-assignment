package com.assignment.utils;

import android.os.Bundle;

public interface OnRecyclerViewItemClickListener {
    void onItemClicked(Bundle bundle);
}
